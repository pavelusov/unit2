var studentsAndPoints = [
		'Алексей Петров', 0,
		'Ирина Овчинникова', 60,
		'Глеб Стукалов', 30,
		'Антон Павлович', 30,
		'Виктория Заровская', 30,
		'Алексей Левенец', 70,
		'Тимур Вамуш', 30,
		'Евгений Прочан', 60,
		'Александр Малов', 0
	],
	maxIndex = -1, max,
	i, sapLength;

// 1 пункт
console.log("");
console.log("1 пункт");
console.log("Список студентов:");
for (i = 0, sapLength = studentsAndPoints.length; i < sapLength; i += 2) {
	console.log("Студент %s набрал %d баллов", studentsAndPoints[i], studentsAndPoints[i + 1]);
}

// 2 пункт
console.log("");
console.log("2 пункт");
console.log("Студент набравший максимальный балл:");
for(i = studentsAndPoints.length - 1; i > 0; i -= 2){ // Начинаем перебериать с конца массива
	if(maxIndex < 0 || studentsAndPoints[i] > max){
		maxIndex = i;
		max = studentsAndPoints[i];
	}
}
console.log("Студент %s набрал максимальный балл %d", studentsAndPoints[maxIndex - 1], max);

// 3 пункт
console.log();
console.log("3 пункт");
console.log("Всего студентов: " + studentsAndPoints.length/2 + ".");
studentsAndPoints.push("Николай Фролов", 0, "Олег Боровой", 0);
console.log();
console.log("Список студентов:");
for (i = 0, sapLength = studentsAndPoints.length; i < sapLength; i += 2) {
	console.log("Студент " + studentsAndPoints[i] + " набрал " + studentsAndPoints[i + 1] + " баллов");
}
console.log("Всего студентов: " + studentsAndPoints.length/2 + ".");

// 4 пункт
console.log();
console.log("4 пункт");
console.log("Список студентов:");
// ДОРАБОТКА ДЗ. Ищем студентов и добавляем по 10 баллов без циклов.
var nikolai = studentsAndPoints.indexOf("Николай Фролов");
if (nikolai !== -1){
	studentsAndPoints[nikolai + 1] += 10;
}
var anton = studentsAndPoints.indexOf("Антон Павлович");
if (anton !== -1){
	studentsAndPoints[anton + 1] += 10;
}
// var dima = studentsAndPoints.indexOf("Дмитрий Фитискин");
// if (dima == -1){
// 	studentsAndPoints[dima + 1] += 10; // Студент Алексей Петров10 набрал 0 баллов
// }

for (i = 0, sapLength = studentsAndPoints.length; i < sapLength; i += 2) {
	console.log("Студент %s набрал %d баллов", studentsAndPoints[i], studentsAndPoints[i + 1]);
}
// 5 пункт
console.log();
// Обнулим
// for (var i = studentsAndPoints.length - 1; i > 0; i -= 2){
// 	studentsAndPoints[i] = 0;
// }
console.log("5 пункт");
console.log("Список студентов не набравших баллы:");
for (i = 0, sapLength = studentsAndPoints.length; i < sapLength; i += 2) {
	if(studentsAndPoints[i + 1] == 0){
		console.log("Студент " + studentsAndPoints[i]);
	}
}
// 6 пункт
console.log();
console.log("Дополнительный пункт");
console.log("Список оставшихся студентов:");
// Удаляем с конца массива. Потому что если удалять с начала, то метод splice
// удаляет из массива studentsAndPoints элементы и сдвигает нижеследующие элементы вверх.
// Поэтому в передыдущих решениях оставались студенты при обнулении.
for (var i = studentsAndPoints.length - 1; i > 0; i -= 2){
	if (studentsAndPoints[i] === 0){
		studentsAndPoints.splice(i - 1, 2);
	}
}
// Выводим список оставшихся студентов
studentsAndPoints.forEach(function (value, i, arr) {
	if(i % 2 == 0){
		console.log("Студент " + value + " набрал " + arr[i + 1] + " баллов");
	}

});

