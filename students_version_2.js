var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0],
    students = [],
    points = [];
for (var i = 0, len = studentsAndPoints.length; i < len; i++ ){
    if (i % 2 == 0){
        students.push(studentsAndPoints[i]);
    }else {
        points.push(studentsAndPoints[i]);
    }
}
console.log(students);
console.log(points);

// 1 Вывести список
console.log("Список студентов:");
for (var i = 0, len = students.length; i < len; i++){
    console.log("Студент %s набрал %d баллов", students[i], points[i]);
}
// 2 Найти студента набравшего наибольшее количество баллов
console.log();
console.log("Студент набравший максимальный балл:");
var max, maxIndex; // Можно не инициализировать maxIndex. Как у вас на уроке imax = -1;.
for (var i = 0, len = points.length; i < len; i++ ){
    if ( points[i] > max || !maxIndex) { // В 1 итерации проверка пройдет по 2 условию.
        // В maxIndex присвоется 0. Что тоже считается !maxIndex.
        max = points[i]; // Но в 1 итерации max инициализируется. А в следующих итерациях  проверяется 1 условие points[i] > max
        maxIndex = i;
    }

}
console.log("Студент %s имеет максимальный балл %d", students[maxIndex], max);
// 3 Новые студенты
students.push("Николай Фролов", "Олег Боровой");
points.push(0, 0);
// 4 Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов
console.log();
var nikolay = students.indexOf("Антон Павлович"); // Добавили баллы без циклов, forEach, find, findIndex и любых других методов которые принимают функцию
points[nikolay] += 10;
var anton = students.indexOf("Николай Фролов"); // indexOf() - принимает строку.
points[anton] += 10;
// 5 Вывести студентов ненабравших баллы
console.log();
console.log("Студенты не набравшие баллов:");
for (var i = 0, len = points.length; i < len; i++ ){
    if (points[i] === 0){
        console.log(students[i]);
    }
}
// 6 Дополнительное задание: Удалить из массива данные о студентах, набравших 0 баллов.
console.log();
for (var i = 0, len = points.length; i < len; i++){
    if (points[i] == 0){
        points.splice(i, 1);
        students.splice(i, 1);
        i--;
    }
}
// Выводим список оставшихся студентов
console.log("Оставшиеся студенты:");
for (var i = 0, len = students.length; i < len; i++){
    console.log("Студент %s набрал %d баллов", students[i], points[i]);
}